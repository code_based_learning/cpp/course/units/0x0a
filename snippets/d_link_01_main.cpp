// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

#include <iostream>
#include <cstdlib>
#include <string>
using std::cout;
using std::endl;
using std::string;

// includes external declarations
#include "d_link_01_extern.hpp"

// Attention!
// This would not be done here, but in a build tool,
// e.g. make, compile this file one by one (just like main) and then
// in a second step, link '01_main' and '01_extern'.
// For simplicity's sake, we can compile and link them together.
// See also the video.
// #include "d_link_01_extern.cpp"

// Since C++20 there are also modules...
// https://en.cppreference.com/w/cpp/language/modules
// or in detail e.g. from here (3-series)
// https://vector-of-bool.github.io/2019/03/10/modules-1.html
// But since the compiler and CMake support is still mediocre,
// we will leave this topic aside for now.

int main()
{
    // external function
    write_something("hello");

    // external class
    C c;
    cout << "2*12=" << c.mal2(12) << ", C::m=" << C::m << endl;

    // Template, as only complete in header file
    D<int> d;
    return EXIT_SUCCESS;
}
