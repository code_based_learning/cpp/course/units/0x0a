// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

#include <iostream>
#include <algorithm>
#include <functional>
#include <array>

using std::cout;
using std::endl;
using std::ostream;
using std::array;
using std::sort;

template <typename T, size_t DIM>                           // (A)
ostream& operator<<(ostream& os, const array<T,DIM>& a) {
    os << "[ ";
    for (auto x : a) {
        os << x << " ";
    }   
    os << "]";
    return os;
}

bool even_odd(int a, int b) {                               // (B)
    return (a%2==b%2) ? (a<=b) : (a%2==0);
}

int main() 
{
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;

    array<int,10> a = {5, 7, 4, 2, 8, 6, 1, 9, 0, 3}; 
    cout << "01|    init.   a=" << a << endl;
    
    sort(a.begin(), a.end());                               // (C)
    cout << "02|    sort    a=" << a << endl;

    // What are the pros and cons of this approach?
    sort(a.begin(), a.end(), even_odd);                     // (D)
    cout << "03|    even    a=" << a << endl;

    struct {
        bool operator()(int a, int b) const { 
            return (a%2==b%2) ? (a>=b) : (a%2==1); 
        }   
    } odd_even;
    sort(a.begin(), a.end(), odd_even);                     // (E)
    cout << "04|    odd     a=" << a << endl;
    
    sort(a.begin(), a.end(), std::greater<>());             // (F)
    cout << "05|    greater a=" << a << endl;
    
                                                            // (G)
    sort(a.begin(), a.end(), [](int a, int b) -> bool { return a <= b; });
    cout << "06|    lambda  a=" << a << endl;
      
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;
    return 0;
}


/* Comments

(A) Array output only.

(B) Comparison of two ints, dividing them into two groups (even, odd)
    and sorts by value within each group.

(C) Sort function without further parameters, only from and to iterators;
    sorted by value (in-place).

(D) Sorts as in (C), but uses the specified function to compare two items.
    Question: What data type is the parameter?

(E) Same as (D), but the comparison operator is given by the object
    and the () operator.

(F) Some comparison operators are predefined, such as greater<>.

(G) This is the shortest version with lambda expressions - will be explained in
    other snippets. Advantage: can be defined directly in place without further
    structures or functions.

*/
 
