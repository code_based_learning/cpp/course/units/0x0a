// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

#include <iostream>
using std::cout;
using std::endl;
using std::string;

int main() 
{
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;

    auto less = [](int a, int b) -> bool { return a < b; }; // (A)
    cout << "01|    2<3? " << less(2,3) << endl;            // (B)
    cout << "02|    3<2? " << less(3,2) << endl;

    auto plus2 = [](int n) -> int { return n+2; };          // (C)
    auto plus3 = [](int n) -> auto { return n+3; };         // (D)
    auto plus4 = [](int n) { return n+4; };                 // (E)

    int n{7};
    cout << "03|    n=" << n 
         << ", n+2=" << plus2(n)                            // (F)
         << ", n+3=" << plus3(n) 
         << ", n+4=" << plus4(n) << endl;

    cout << "-----" << endl;

    int m{3};
    auto plusm = [=](int n) { return n+m; };                // (G)

    cout << "04|    n=" << n << ", m=" << m
         << ", n+m=" << plusm(n) << endl;

    m = 100;                                                // (H)
    cout << "05|    n=" << n << ", m=" << m
         << ", n+m=" << plusm(n) << endl;
    // auto pluscm = [=](int n) { m=2; return n+m; };
    // auto pluscm = [=](int n) mutable { m=2; return n+m; };

    m = 3;
    auto plusrefm = [&m](int n) { return n+m; };            // (I)

    cout << "06|    n=" << n << ", m=" << m
         << ", n+m=" << plusrefm(n) << endl;

    m = 100;                                                // (J)
    cout << "07|    n=" << n << ", m=" << m
         << ", n+m=" << plusrefm(n) << endl;

    cout << "-----" << endl;



    string A{"<"}, B{">"};
    auto compose = [&](const string& s) { return A+s+B; };  // (K)

    cout << "08|    compose:'" << compose("line") << "'" << endl;

    cout << "-----" << endl;

    struct S {
        int n{0};
        void f() {
            [&]() -> void { n = 1;} ();                     // (L)
            cout << "09|    n=" << n << endl;
        }
        void g() {
            // [*this]() -> void { n = 2;} ();
            [*this]() mutable -> void { n = 2;} ();         // (M)
            cout << "10|    n=" << n << endl;
        }
        void h() {
            [this]() mutable -> void { n = 3;} ();          // (N)
            cout << "11|    n=" << n << endl;
        }
        // check? also change without 'mutable'?
        void i() {
            [this]() -> void { n = 4;} ();
            cout << "12|    n=" << n << endl;
        }
    };
    S().f();
    S().g();
    S().h();
    S().i();

    cout << "-----" << endl;

    auto print_type = []<typename T>(T t) {                 // (O)
        std::cout << "13|    type name: " << typeid(T).name() << endl;
    };
    print_type(1);
    print_type(1.0f);
    print_type("Hello, Template Lambda");

    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;
    return 0;
}

/* Comments

(A) This is a lambda expression. It consists of three parts:
    - The [] brackets at the beginning, explanation follows.
    - The parameters a and b
    - The {} statement block itself.
    Depending on the syntax (we have two variants), we also need "->" and the return type.
    This expression calculates "a<b". The derivation of the return type (bool)
    can be left to the compiler with "auto", see (D).
    The syntax "->" follows the mathematical way of writing x->x^2.

(B) Use the lambda expression like a function.

(C) Another lambda expression, it calculates "n+2".

(D) Same as (C), except that the result type is determined automatically.

(E) Different syntax, without "->". And the type is derived from the return expression.

(F) Like (B).

(G) The lambda expression function uses "m" but which "m" is used?
    The expression "=" in the [] indicates, how and which "external" variables can be seen.
    Here "=" says that "m" is copied and this copy is known within the expression.

(H) Attention: the "m" in (G) is not always the current value, but the copy made
    at or before the first use!
    Also, all values are const, unless you spend 'mutable' to remove const-ness.

(I) Like (H), except that "m" is now a reference...

(J) ... and therefore reflects the current value in the calculation.

(K) You can also generally use all external values per reference by using
    "&" in the [], here for example the strings.

(L) Lambda with direct call... but here it is the capture clause:
    - capture '&' -> everything by reference, even this.

(M) capture *this -> This is a copy of *this, but const (see (H)),
    hence the extra 'mutable', so that the copy can be modified.
    The original-n remains, of course.

(N) capture this without copy -> change ok.
    Check: 'this' is only const if the member function is const.
    So the 'mutable' in 'h' does not change anything.

    Overall, capture this has changed over time, see e.g.
    - https://en.cppreference.com/w/cpp/language/lambda
    - https://www.nextptr.com/tutorial/ta1430524603/capture-this-in-lambda-expression-timeline-of-change

(O) Generic lambda. The name of the typeid is not necessarily human-readable, it is the internal name,
    e.g. for name mangling.

*/
