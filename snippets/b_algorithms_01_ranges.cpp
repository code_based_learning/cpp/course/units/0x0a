// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

#include <iostream>
#include <algorithm>
#include <array>
#include <vector>
#include <iostream>
#include <ranges>

using std::cout;
using std::endl;
using std::ostream;
using std::array;
using std::vector;
using std::sort;
using std::views::filter;
using std::views::transform;

/*

In C++20 so-called ranges have been added, which contain information about the range,
e.g. start and end. There are now many algorithms for ranges, here is just a small teaser.

see https://en.cppreference.com/w/cpp/ranges
*/

template <typename T>                                       // (A)
ostream& operator<<(ostream& os, const vector<T>& a) {
    os << "[ ";
    for (auto x : a) {
        os << x << " ";
    }   
    os << "]";
    return os;
}

int main() 
{
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;

    vector<int> v0 = {5, 7, 4, 2, 8, 6, 1, 9, 0, 3, 5};
    cout << "01|    v0=" << v0 << endl;

    auto v1 = v0 | std::views::filter([](int n){ return n % 2 == 0; }); // (B)
    cout << "02|    v1=[ ";
    for(auto& n : v1)                                       // (C)
        cout << n << " ";
    cout << "]" << endl;

    auto v2 = vector(v1.begin(),v1.end());                  // (D)
    cout << "       v2=" << v2 << endl;

    auto v3 = v0 | filter([](auto n){ return n % 2 == 0; })
        | transform([](auto i) { return i * i; });          // (E)
    cout << "03|    v3=" << vector(v3.begin(),v3.end()) << endl;

    // other languages: v0.filter { n -> n % 2 == 0 }.transform { it * it } ...

    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;
    return 0;
}


/* Comments

(A) Output.

(B) Note the pipe operator '|'.

(C) You can traverse the result of such a filter operation.

(D) Convert it into a vector. This does not make sense in general, it is just an example.

(E) Chaining of the pipes, including 'auto'.

*/
