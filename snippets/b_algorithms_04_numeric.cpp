// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

#include <iostream>
#include <algorithm>
#include <functional>
#include <array>
#include <vector>
#include <iostream>
#include <numeric>

using std::cout;
using std::endl;
using std::ostream;
using std::array;
using std::vector;
using std::sort;

template <typename T, size_t DIM>                           // (A)
ostream& operator<<(ostream& os, const array<T,DIM>& a) {
    os << "[ ";
    for (auto x : a) {
        os << x << " ";
    }   
    os << "]";
    return os;
}

int main() 
{
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;

    array<int,6> a = {  }; 
    cout << "01|    a=" << a << endl;

    cout << "-----" << endl;
    
    // numerics

    std::iota(a.begin(), a.end(),1);                        // (B)
    cout << "02|    iota from 1, a=" << a << endl;
    
    int sum_1_3 = std::accumulate(a.begin()+1, a.begin()+3+1,101);
    cout << "03|    sum idx 1..3 +101: " << sum_1_3 << endl;// (C)

    array<int,6> s = {  }; 
    std::partial_sum(a.begin(), a.end(),s.begin());         // (D)
    cout << "04|    partial sums, s=" << s << endl;

    cout << "-----" << endl;

    std::string msg("hello kurs");
    std::transform(msg.begin(), msg.end(), msg.begin(),     // (E)
                   [](unsigned char c) -> unsigned char { return (char)std::toupper(c); });
    cout << "05|    upper msg: '" << msg << "'" << endl;
      
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;
    return 0;
}


/* Comments

(A) Output.

(B) Create an ascending sequence.

(C) From Pos. 1 to 3 (2+3+4), then add 101.

(D) Calculate partial sums (1,1+2,1+2+3,...), depending on the content in a.

(E) Transform a sequence of elements, here in capital letters and store it again.

*/
