// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

#include <iostream>
#include <functional>
#include <cmath>
using std::cout;
using std::endl;
using std::string;
using std::function;
using std::asin;

typedef double (*function_t)(double x);                     // (A)

double evalf(function_t f, double x) {                      // (B)
	return f(x);
}

double x2(double x) {                                       // (C)
    return x*x; 
}

double x3(double x) { 
    return x*x*x; 
}

double evalg(function<double(double)> g, double x) {        // (D)
	return g(x);
}

int main() 
{
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;
    
    cout << "01|    3^2=" << evalf(x2,3.0) << endl;         // (E)
    
    function_t f3{x3};                                      // (F)
    cout << "02|    2^3=" << evalf(f3,2.0) << endl;         // (G) 

    auto l4a = [](double x){return x*x*x*x;};               // (H)
    cout << "03|    1^4=" << evalf(l4a,1.0) << endl;
    
    function_t l4b = [](double x) -> double {return x*x*x*x;};
    cout << "04|    1^4=" << evalf(l4b,1.0) << endl;

    cout << "05|    1^4="                                   // (I)
         << evalf([](double x) -> double {return x*x*x*x;}, 1.0) << endl;

    cout << "-----" << endl;

    function_t g1 = asin;                                   // (J)
    cout << "06|    2asin(0)=" << 2*evalg(g1,1.0) << endl;

    function<double(double)> g2 = (function_t)asin;         // (K)
    cout << "07|    2asin(0)=" << 2*evalg(g2,1.0) << endl;

    double(*g3)(double) = asin;                             // (L)
    cout << "08|    2asin(0)=" << 2*evalg(g3,1.0) << endl;

    function<double(double)> g4 = (double(*)(double))asin;  // (M)
    cout << "09|    2asin(0)=" << 2*evalg(g4,1.0) << endl;

    function<double(double)> g5 = [](double x) -> double {return asin(x);};
    cout << "10|    2asin(0)=" << 2*evalg(g5,1.0) << endl;

    cout << "11|    2asin(0)=" 
         << 2*evalg([](double x) -> double {return asin(x);},1.0) << endl;
      
    cout << endl << "--- " << __FILE__ << " ---" << endl << endl;
    return 0;
}

/* Comments

(A) This is the function pointer type. Variables of this type can be pointers to
    functions (as in vtbl, for instance), they essentially contain the address of
    the function's start in memory. The data type is "function_t".
    With this datatype we can now define functions, which get a function pointer,
    e.g. for evaluation...

(B) ... as for the function evalf, which is to apply the function f to the argument x.

(C) This will be one of our polynom functions, x->x^2.

(D) Since the evaluation of a function is a common application, there are predefined templates,
    e.g. "function". <return type(arg types)>

(E) Evaluation of the function x^2 at x=3.0. Here the function pointer is the parameter.

(F) A variable "f3", as a copy of the function x^3 (copy of the pointer, not the function code)...

(G) ... which we can also evaluate.

(H) There is no explicit data type for lambda expressions, but a lambda expression can easily
    be converted to a function pointer (calling evalf) or into the data type "function_t".

(I) Of course we can also use the lambda expression directly.

(J) Converting to function_t works.

(K) The problem here is that the compiler cannot select the "right" function
    from the various possible overloads, even if it is obvious to us.
    std::function<> is a high-level wrapper which can store callable entities, and
    a function pointer is a 'low level entity' and it cannot be converted implicitly
    but a cast works.

(M) Same here.

(L) shows that the use of typedefs makes the code more readable.

*/
