// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

                                                            // (A)
#if !defined(C_FUNCTIONS)
#define C_FUNCTIONS 1

extern int n;                                               // (B)

void write_something(const string& s);                      // (C)

class C                                                     // (D)
{
public:
    C();
    
    int mal2(int n);
    
    static int m;
};
        
template <typename T>                                       // (E)
class D
{
public:
    D() { cout << "D::ctor" << endl; }
};

#endif

/* Comments

(A) include-guard: prevents double include.

(B) There is a variable of type int called n.

(C) Analogous to (B), there is a function write_something.

(D) In class C, the members are defined external.

(E) Analogous to (D).

*/
 

