# C++ Course for experienced Programmers

## Content

- Snippets can be found in `snippets`,
- tasks and blank solutions in `exercises`, and
- proposed solutions in `exercises/proposed_solutions`.

## Comments

Please feel free to send constructive comments and additions to [me](mailto:cpp@codebasedlearning.dev).
